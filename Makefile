APP=hgen
BIN=build/$(APP)
DC=dmd
SRC=$(shell find source -name "*.d")
STYLES=$(shell find strings -name "*")
DESTDIR=
PREFIX=usr/local

.PHONY: all doc unittests install uninstall clean

all: $(BIN)

$(BIN): $(SRC) source/version $(STYLES) dub.sdl
	dub build --compiler=$(DC) --force -v

doc: $(BIN)
	$(BIN) source/

unittests:
	dub test --compiler=$(DC) -v

install:
	install $(BIN) $(DESTDIR)/$(PREFIX)/bin/

uninstall:
	rm -f $(DESTDIR)/$(PREFIX)/bin/$(APP)

clean:
	rm -rf build/ doc/

