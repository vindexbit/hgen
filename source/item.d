/* This file is part of the 'hgen' project.
 *
 * Copyright (c) 2014-2024
 *     Economic Modeling Specialists, Intl.; the D Community.
 *
 * Distributed under the Boost Software License, Version 1.0.
 *
 * You should have received a copy of the Boost Software License
 * along with this program. If not, see <http://www.boost.org/LICENSE_1_0.txt>.
 * This file is offered as-is, without any warranty.
 */

module item;

import formatter;
import std.algorithm;
import std.array: appender, empty, array;
import dparse.ast;
import std.string: format;


struct Item {
    string url;
    string name;
    string summary;
    string type;

    /// AST node of the item. Only used for functions at the moment.
    const ASTNode node;
}


struct Members {
    Item[] aliases;
    Item[] classes;
    Item[] enums;
    Item[] functions;
    Item[] interfaces;
    Item[] structs;
    Item[] templates;
    Item[] values;
    Item[] variables;

    Item[] publicImports;

    /// Write the list of public imports declared in a module.
    void writePublicImports(R, Writer)(ref R dst, Writer wr) {
        if (publicImports.empty) return;

        void innWriteImports() {
            foreach(imp; publicImports) {
                wr.writeListItem(
                    dst,
                    {
                        if (imp.url is null) {
                            dst.put(imp.name);
                        } else {
                            wr.writeLink(dst, imp.url, { dst.put(imp.name); });
                        }
                    }
                );
            }
        }

        wr.writeSection(
            dst,
            { wr.writeList(dst, "Public imports", &innWriteImports); },
            "imports"
        );
    }

    /// Write the table of members for a class/struct/module/etc.
    void write(R, Writer)(ref R dst, Writer wr) {
        if (aliases.empty && classes.empty && enums.empty && functions.empty
            && interfaces.empty && structs.empty && templates.empty
            && values.empty && variables.empty) {
            return;
        }
        wr.writeSection(dst, {
            if (!enums.empty)      wr.writeItems(dst, enums, "Enums");
            if (!aliases.empty)    wr.writeItems(dst, aliases, "Aliases");
            if (!variables.empty)  wr.writeItems(dst, variables, "Variables");
            if (!functions.empty)  wr.writeItems(dst, functions, "Functions");
            if (!structs.empty)    wr.writeItems(dst, structs, "Structs");
            if (!interfaces.empty) wr.writeItems(dst, interfaces, "Interfaces");
            if (!classes.empty)    wr.writeItems(dst, classes, "Classes");
            if (!templates.empty)  wr.writeItems(dst, templates, "Templates");
            if (!values.empty)     wr.writeItems(dst, values, "Values");
        }, "members");
    }
}
