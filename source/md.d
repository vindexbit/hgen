/*******************************************************************************
 * Markdown parser implementation.
 *
 * Copyright: (c) 2012-2019 RejectedSoftware e.K. and the D community
 * License: Subject to the terms of the MIT license.
 * Repository: https://github.com/dlang-community/dmarkdown
 *
 * This library was forked and modified in 2021-2022 for the `hgen` project.
 * hgen: https://gitlab.com/os-18/hgen
 * Author: Eugene 'Vindex' Stulin <tech.vindex@gmail.com>
 *
 * MIT License (Expat version)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

module md;

import std.algorithm;
import std.array;
import std.ascii;
import std.conv;
import std.exception;
import std.format;
import std.uni;
import std.utf;
import core.exception;
import std.range;
import std.string;
import std.stdio;

alias UrlFilterFn = string delegate(string urlOrPath, bool isImage);
alias ProcessCodeFn = string delegate(string) @safe nothrow;


enum MarkdownFlags {
    none = 0,
    keepLineBreaks = 1 << 0,
    backtickCodeBlocks = 1 << 1,
    noInlineHtml = 1 << 2,
    //noLinks = 1<<3,
    //allowUnsafeHtml = 1<<4,
    /// If used,
    /// subheadings are underlined by stars ('*') instead of dashes ('-')
    // alternateSubheaders = 1 << 5,
    /// If used, '_' may not be used for emphasis ('*' may still be used)
    disableUnderscoreEmphasis = 1 << 6,
    supportTables = 1 << 7,
    vanillaMarkdown = none,
    forumDefault = keepLineBreaks | backtickCodeBlocks | noInlineHtml,
    githubInspired = backtickCodeBlocks | supportTables,
}


private enum LineType {
    Undefined,
    Blank,
    Plain,
    Hline,
    AtxHeader,
    SetextHeader,
    UList,
    OList,
    HtmlBlock,
    CodeBlockDelimiter,
    Table,
}


private enum BlockType {
    Plain,
    Text,
    Paragraph,
    Header,
    OList,
    UList,
    ListItem,
    Code,
    Quote,
    Table,
    TableRow,
    TableHeader,
    TableData,
}


private struct LinkRef {
    string id;
    string url;
    string title;
}


private bool edgesAreEqual(string l, char c) pure @safe {
    return l[0] == c && l[$-1] == c;
}


private string extractUrl(ref string ln) pure @safe {
    string url;
    if (ln.startsWith("<")) {
        auto idx = ln.indexOfCT('>');
        enforce(idx >= 0, "No reference line.");
        url = ln[1 .. idx];
        ln = ln[idx + 1 .. $];
    } else {
        auto idx = ln.indexOfCT(' ');
        if (idx > 0) {
            url = ln[0 .. idx];
            ln = ln[idx + 1 .. $];
        } else {
            idx = ln.indexOfCT('\t');
            if (idx < 0) {
                url = ln;
                ln = ln[$ .. $];
            } else {
                url = ln[0 .. idx];
                ln = ln[idx+1 .. $];
            }
        }
    }
    ln = stripLeft(ln);
    return url;
}


private LinkRef extractLinkRef(ref string ln) pure @safe {
    enforce(!isLineIndented(ln), "No reference line.");

    ln = strip(ln);
    enforce(ln.startsWith("["), "No reference line.");
    ln = ln[1 .. $];  // without '['

    auto idx = ln.indexOf("]:");
    enforce(idx >= 0, "No reference line.");
    string refid = ln[0 .. idx];
    ln = ln[idx + 2 .. $].stripLeft;

    string url = extractUrl(ln);

    string title;
    if (ln.length >= 3) {
        if (ln[0] == '(' && ln[$ - 1] == ')' ||
            edgesAreEqual(ln, '"') || edgesAreEqual(ln, '\'')
        ) {
            title = ln[1 .. $-1];
        }
    }
    return LinkRef(refid, url, title);
}


private LinkRef[string] scanForReferences(ref string[] lines) pure @safe {
    LinkRef[string] ret;
    bool[size_t] reflines;
    // line must not be indented
    foreach (i, ln; lines) {
        try {
            auto r = extractLinkRef(ln);
            ret[toLower(r.id)] = r;
        } catch (Exception) {
            continue;
        }
        reflines[i] = true;
    }
    // remove all lines containing references
    auto nonreflines = appender!(string[])();
    nonreflines.reserve(lines.length);
    foreach (i, ln; lines) {
        if (i !in reflines) {
            nonreflines.put(ln);
        }
    }
    lines = nonreflines.data();
    return ret;
}


final class MarkdownSettings {
    /// Controls the capabilities of the parser.
    MarkdownFlags flags = MarkdownFlags.vanillaMarkdown;

    /// Heading tags will start at this level.
    size_t headingBaseLevel = 1;

    /// Called for every link/image URL to perform arbitrary transformations.
    string delegate(string urlOrPath, bool isImage) urlFilter;

    /***************************************************************************
     * An optional delegate to post-process code blocks and inline code.
     * Useful to e.g. add code highlighting.
     */
    string delegate(string) @safe nothrow processCode = null;
}


deprecated string convertMarkdownToHTML(
    string markdownText, MarkdownSettings settings = null
) @trusted {
    auto handler = new MarkdownHandler(markdownText);
    if (settings !is null) {
        handler.flags = settings.flags;
        handler.setHeadingBaseLevel(settings.headingBaseLevel);
        handler.urlFilter = settings.urlFilter;
        handler.processCode = settings.processCode;
    }
    return handler.convertToHTML();
}


deprecated string convertMarkdownToHTML(
    string markdownText, MarkdownFlags flags
) @trusted {
    auto handler = new MarkdownHandler(markdownText);
    handler.setFlags(flags);
    return handler.convertToHTML();
}


private struct Line {
    LineType type;
    IndentType[] indent;
    string text;
    string unindented;

    string unindent(size_t n) pure @safe {
        assert(n <= indent.length);
        string ln = text;
        foreach (i; 0 .. n) {
            final switch (indent[i]) {
                case IndentType.White:
                    ln = (ln[0] == ' ') ? ln[4 .. $] : ln[1 .. $];
                    break;
                case IndentType.Quote:
                    ln = ln.stripLeft()[1 .. $];
                    break;
            }
        }
        return ln;
    }
}


class MarkdownHandler {
    this(string markdownText) @safe {
        this.markdownText = markdownText;
    }

    void setProcessCodeFunction(ProcessCodeFn process) {
        this.processCode = process;
    }

    void setHeadingBaseLevel(size_t newHeadingBaseLevel) {
        this.headingBaseLevel = newHeadingBaseLevel;
    }

    void setFlags(MarkdownFlags newFlags) {
        this.flags = newFlags;
    }

    void disableUnderscoreEmphasis() {
        this.flags |= MarkdownFlags.disableUnderscoreEmphasis;
    }

    string convertToHTML() {
        string[] allLines = std.string.splitLines(this.markdownText);
        LinkRef[string] links = scanForReferences(allLines);
        Line[] lines = this.parseLines(allLines);
        Block rootBlock;
        this.parseBlocks(rootBlock, lines);
        auto dst = appender!string();
        this.writeBlock(dst, rootBlock, links);
        return dst.data;
    }

    void setUrlFilterFunction(UrlFilterFn filter) {
        this.urlFilter = filter;
    }

private:

    Line[] parseLines(ref string[] lines) @safe {
        Line[] ret;
        while (!lines.empty) {
            Line lninfo;
            lninfo.text = lines.front;
            lines.popFront();
            determineIndent(lninfo);
            lninfo.type = determineType(lninfo.unindented);
            ret ~= lninfo;
        }
        return ret;
    }

    void determineIndent(ref Line lninfo) @safe {
        auto ln = lninfo.text.idup;
        while (ln.length > 0) {
            if (ln[0] == '\t') {
                lninfo.indent ~= IndentType.White;
                ln.popFront();
            } else if (ln.startsWith("    ")) {
                lninfo.indent ~= IndentType.White;
                ln.popFrontN(4);
            } else {
                ln = ln.stripLeft();
                if (ln.startsWith(">")) {
                    lninfo.indent ~= IndentType.Quote;
                    ln.popFront();
                } else {
                    break;
                }
            }
        }
        lninfo.unindented = ln;
    }

    LineType determineType(string ln)
    pure @safe {
        alias MF = MarkdownFlags;
        if ((flags & MF.backtickCodeBlocks) && isCodeBlockDelimiter(ln)) {
            return LineType.CodeBlockDelimiter;
        } else if (isAtxHeaderLine(ln)) {
            return LineType.AtxHeader;
        } else if (isSetextHeaderLine(ln, '-') || isSetextHeaderLine(ln, '=')) {
            return LineType.SetextHeader;
        } else if ((flags & MF.supportTables) && isTableRowLine(ln)) {
            return LineType.Table;
        } else if (isHlineLine(ln)) {
            return LineType.Hline;
        } else if (isOListLine(ln)) {
            return LineType.OList;
        } else if (isUListLine(ln)) {
            return LineType.UList;
        } else if (isLineBlank(ln)) {
            return LineType.Blank;
        } else if (!(flags & MF.noInlineHtml) && isHtmlBlockLine(ln)) {
            return LineType.HtmlBlock;
        }
        return LineType.Plain;
    }

    void parseBlocks(ref Block root,
                     ref Line[] lines,
                     IndentType[] baseIndent = null)
    pure @safe {
        if (baseIndent.empty) {
            root.type = BlockType.Text;
        } else if (baseIndent[$ - 1] == IndentType.Quote) {
            root.type = BlockType.Quote;
        }

        while (!lines.empty) {
            auto ln = lines.front;

            if (ln.type == LineType.Blank) {
                lines.popFront();
                continue;
            }

            if (ln.indent != baseIndent) {
                if (ln.indent.length < baseIndent.length ||
                    ln.indent[0 .. baseIndent.length] != baseIndent
                ) {
                    return;
                }

                auto cindent = baseIndent ~ IndentType.White;
                if (ln.indent == cindent) {
                    Block cblock;
                    cblock.type = BlockType.Code;
                    while (
                        !lines.empty &&
                        lines.front.indent.length >= cindent.length &&
                        lines.front.indent[0 .. cindent.length] == cindent
                    ) {
                        cblock.text ~= lines.front.unindent(cindent.length);
                        lines.popFront();
                    }
                    root.blocks ~= cblock;
                } else {
                    Block subblock;
                    this.parseBlocks(
                        subblock,
                        lines,
                        ln.indent[0 .. baseIndent.length + 1]
                    );
                    root.blocks ~= subblock;
                }
                return;
            }

            Block b;
            void processPlain() {
                b.type = BlockType.Paragraph;
                b.text = skipText(lines, baseIndent);
            }

            final switch (ln.type) {
                case LineType.Undefined:
                    assert(false);
                case LineType.Blank:
                    assert(false);
                case LineType.Plain:
                    if (lines.length >= 2 && lines[1].type == LineType.SetextHeader) {
                        auto setln = lines[1].unindented;
                        b.type = BlockType.Header;
                        b.text = [ln.unindented];
                        b.headerLevel = setln.strip()[0] == '=' ? 1 : 2;
                        lines.popFrontN(2);
                    } else {
                        processPlain();
                    }
                    break;
                case LineType.Hline:
                    b.type = BlockType.Plain;
                    b.text = ["<hr>"];
                    lines.popFront();
                    break;
                case LineType.AtxHeader:
                    b.type = BlockType.Header;
                    string hl = ln.unindented;
                    b.headerLevel = 0;
                    while (hl.length > 0 && hl[0] == '#') {
                        b.headerLevel++;
                        hl = hl[1 .. $];
                    }
                    while (hl.length > 0 && (hl[$ - 1] == '#' || hl[$ - 1] == ' ')) {
                        hl = hl[0 .. $ - 1];
                    }
                    b.text = [hl];
                    lines.popFront();
                    break;
                case LineType.SetextHeader:
                    lines.popFront();
                    break;
                case LineType.UList:
                case LineType.OList:
                    b.type = ln.type == LineType.UList ? BlockType.UList
                                                       : BlockType.OList;
                    auto itemindent = baseIndent ~ IndentType.White;
                    bool firstItem = true, paraMode = false;
                    while (
                        !lines.empty &&
                        lines.front.type == ln.type &&
                        lines.front.indent == baseIndent
                    ) {
                        Block itm;
                        itm.text = skipText(lines, itemindent);
                        itm.text[0] = removeListPrefix(itm.text[0], ln.type);

                        // emit <p></p> if there are blank lines
                        // between the items
                        if (firstItem && !lines.empty
                            && lines.front.type == LineType.Blank) {
                            paraMode = true;
                        }
                        firstItem = false;
                        if (paraMode) {
                            Block para;
                            para.type = BlockType.Paragraph;
                            para.text = itm.text;
                            itm.blocks ~= para;
                            itm.text = null;
                        }

                        this.parseBlocks(itm, lines, itemindent);
                        itm.type = BlockType.ListItem;
                        b.blocks ~= itm;
                    }
                    break;
                case LineType.HtmlBlock:
                    int nestlevel = 0;
                    auto starttag = parseHtmlBlockLine(ln.unindented);
                    if (!starttag.isHtmlBlock || !starttag.open)
                        break;

                    b.type = BlockType.Plain;
                    while (!lines.empty) {
                        auto frontIndLen = lines.front.indent.length;
                        auto baseIndLen = baseIndent.length;
                        if (frontIndLen < baseIndLen) {
                            break;
                        }
                        if (lines.front.indent[0 .. baseIndLen] != baseIndent) {
                            break;
                        }

                        auto str = lines.front.unindent(baseIndent.length);
                        auto taginfo = parseHtmlBlockLine(str);
                        b.text ~= lines.front.unindent(baseIndent.length);
                        lines.popFront();
                        if (taginfo.isHtmlBlock
                            && taginfo.tagName == starttag.tagName
                        ) {
                            nestlevel += taginfo.open ? 1 : -1;
                        }
                        if (nestlevel <= 0) {
                            break;
                        }
                    }
                    break;
                case LineType.CodeBlockDelimiter:
                    lines.popFront();  // TODO: get language from line
                    b.type = BlockType.Code;
                    while (!lines.empty) {
                        if (lines.front.indent.length < baseIndent.length) {
                            break;
                        }
                        if (lines.front.indent[0 .. baseIndent.length] != baseIndent) {
                            break;
                        }
                        if (lines.front.type == LineType.CodeBlockDelimiter) {
                            lines.popFront();
                            break;
                        }
                        b.text ~= lines.front.unindent(baseIndent.length);
                        lines.popFront();
                    }
                    break;
                case LineType.Table:
                    lines.popFront();
                    // Can this be a valid table (is there a next line
                    // that could be a header separator)?
                    if (lines.empty) {
                        processPlain();
                        break;
                    }
                    Line lnNext = lines.front;
                    immutable bool isTableHeader =
                        lnNext.type ==
                            LineType.Table &&
                            lnNext.text.indexOf(" -") >= 0 &&
                            lnNext.text.indexOf("- ") >= 0 &&
                            lnNext.text.allOf("-:| ");
                    if (!isTableHeader) {
                        // Not a valid table header,
                        // so let's assume it's plain markdown
                        processPlain();
                        break;
                    }
                    b.type = BlockType.Table;
                    // Parse header
                    b.blocks ~= splitTableRow!(BlockType.TableHeader)(ln);
                    // Parse table rows
                    lines.popFront();
                    while (!lines.empty) {
                        ln = lines.front;
                        if (ln.type != LineType.Table)
                            break; // not a table row, so let's assume it's the end of the table
                        b.blocks ~= splitTableRow(ln);
                        lines.popFront();
                    }
                    break;
            }
            root.blocks ~= b;

        }
    }

    void writeBlock(R)(ref R dst, ref const Block block, LinkRef[string] links)
    do {
        final switch (block.type) {
        case BlockType.Plain:
            foreach (ln; block.text) {
                dst.put(ln);
                dst.put("\n");
            }
            foreach (b; block.blocks) {
                this.writeBlock(dst, b, links);
            }
            break;
        case BlockType.Text:
            writeMarkdownEscaped(dst, block, links);
            foreach (b; block.blocks) {
                this.writeBlock(dst, b, links);
            }
            break;
        case BlockType.Paragraph:
            assert(block.blocks.length == 0);
            dst.put("<p>");
            writeMarkdownEscaped(dst, block, links);
            dst.put("</p>\n");
            break;
        case BlockType.Header:
            assert(block.blocks.length == 0);
            auto hlvl = block.headerLevel + this.headingBaseLevel - 1;
            dst.formattedWrite(
                "<h%s id=\"%s\">", hlvl, block.text[0].asSlug
            );
            assert(block.text.length == 1);
            writeMarkdownEscaped(dst, block.text[0], links);
            dst.formattedWrite("</h%s>\n", hlvl);
            break;
        case BlockType.OList:
            dst.put("<ol>\n");
            foreach (b; block.blocks) {
                this.writeBlock(dst, b, links);
            }
            dst.put("</ol>\n");
            break;
        case BlockType.UList:
            dst.put("<ul>\n");
            foreach (b; block.blocks) {
                this.writeBlock(dst, b, links);
            }
            dst.put("</ul>\n");
            break;
        case BlockType.ListItem:
            dst.put("<li>");
            writeMarkdownEscaped(dst, block, links);
            foreach (b; block.blocks) {
                this.writeBlock(dst, b, links);
            }
            dst.put("</li>\n");
            break;
        case BlockType.Code:
            assert(block.blocks.length == 0);
            dst.put("<pre class=\"prettyprint\"><code>");
            if (this.processCode is null) {
                foreach (ln; block.text) {
                    filterHTMLEscape(dst, ln);
                    dst.put("\n");
                }
            } else {
                auto temp = appender!string();
                foreach (ln; block.text) {
                    filterHTMLEscape(temp, ln);
                    temp.put("\n");
                }
                dst.put(this.processCode(temp.data));
            }
            dst.put("</code></pre>");
            break;
        case BlockType.Quote:
            dst.put("<blockquote>");
            writeMarkdownEscaped(dst, block, links);
            foreach (b; block.blocks) {
                this.writeBlock(dst, b, links);
            }
            dst.put("</blockquote>\n");
            break;
        case BlockType.Table:
            assert(block.blocks.length > 0);
            assert(block.blocks[0].type == BlockType.TableRow);
            dst.put("<table>\n<tr>");
            foreach (b; block.blocks[0].blocks) {
                assert(b.type == BlockType.TableHeader);
                dst.put("<th>");
                writeMarkdownEscaped(dst, b.text[0], links);
                dst.put("</th>");
            }
            dst.put("</tr>\n");
            if (block.blocks.length > 1) {
                foreach (row; block.blocks[1 .. $]) {
                    assert(row.type == BlockType.TableRow);
                    dst.put("<tr>");
                    foreach (b; row.blocks) {
                        assert(b.type == BlockType.TableData);
                        dst.put("<td>");
                        writeMarkdownEscaped(dst, b.text[0], links);
                        dst.put("</td>");
                    }
                    dst.put("</tr>\n");
                }
            }
            dst.put("</table>\n");
            break;
        case BlockType.TableRow:
        case BlockType.TableData:
        case BlockType.TableHeader:
            assert(0);
        }
    }

    void writeMarkdownEscaped(R)(ref R dst,
                                 ref const Block block,
                                 in LinkRef[string] links) {
        auto lines = cast(string[]) block.text;
        auto text = this.flags & MarkdownFlags.keepLineBreaks
            ? lines.join("<br>") : lines.join("\n");
        writeMarkdownEscaped(dst, text, links);
        if (lines.length) {
            dst.put("\n");
        }
    }

    void writeMarkdownEscaped(R)(ref R dst,
                                 string ln,
                                 in LinkRef[string] linkrefs) {
        string filterLink(string lnk, bool isImage) {
            return this.urlFilter ? this.urlFilter(lnk, isImage) : lnk;
        }

        bool br = ln.endsWith("  ");
        while (ln.length > 0) {
            switch (ln[0]) {
            default:
                dst.put(ln[0]);
                ln = ln[1 .. $];
                break;
            case '\\':
                if (ln.length >= 2) {
                    switch (ln[1]) {
                    default:
                        dst.put(ln[0 .. 2]);
                        ln = ln[2 .. $];
                        break;
                    case '\'', '`', '*', '_', '{', '}', '[', ']',
                        '(', ')', '#', '+', '-', '.', '!':
                        dst.put(ln[1]);
                        ln = ln[2 .. $];
                        break;
                    }
                } else {
                    dst.put(ln[0]);
                    ln = ln[1 .. $];
                }
                break;
            case '_':
                if (this.flags & MarkdownFlags.disableUnderscoreEmphasis) {
                    dst.put(ln[0]);
                    ln = ln[1 .. $];
                    break;
                }
                goto case;
            case '*':
                string text;
                if (auto em = parseEmphasis(ln, text)) {
                    if (em == 1) {
                        dst.put("<em>");
                    } else if (em == 2) {
                        dst.put("<strong>");
                    } else {
                        dst.put("<strong><em>");
                    }
                    filterHTMLEscape(
                        dst, text, HTMLEscapeFlags.escapeMinimal
                    );
                    if (em == 1) {
                        dst.put("</em>");
                    } else if (em == 2) {
                        dst.put("</strong>");
                    } else {
                        dst.put("</strong></em>");
                    }
                } else {
                    dst.put(ln[0]);
                    ln = ln[1 .. $];
                }
                break;
            case '`':
                string code;
                if (parseInlineCode(ln, code)) {
                    // code prettyprint lose pretty formating
                    // dst.put("<code class=\"prettyprint\">");
                    dst.put(`<pre class="prettyprint">`);
                    if (this.processCode is null) {
                        filterHTMLEscape(
                            dst, code, HTMLEscapeFlags.escapeMinimal
                        );
                    } else {
                        auto temp = appender!string();
                        filterHTMLEscape(
                            temp, code, HTMLEscapeFlags.escapeMinimal
                        );
                        dst.put(this.processCode(temp.data));
                    }
                    dst.put("</pre>");
                } else {
                    dst.put(ln[0]);
                    ln = ln[1 .. $];
                }
                break;
            case '[':
                Link link;
                if (parseLink(ln, link, linkrefs)) {
                    dst.put("<a href=\"");
                    filterHTMLAttribEscape(dst, filterLink(link.url, false));
                    dst.put("\"");
                    if (link.title.length) {
                        dst.put(" title=\"");
                        filterHTMLAttribEscape(dst, link.title);
                        dst.put("\"");
                    }
                    dst.put(">");
                    writeMarkdownEscaped(dst, link.text, linkrefs);
                    dst.put("</a>");
                } else {
                    dst.put(ln[0]);
                    ln = ln[1 .. $];
                }
                break;
            case '!':
                Link link;
                if (parseLink(ln, link, linkrefs)) {
                    dst.put("<img src=\"");
                    filterHTMLAttribEscape(dst, filterLink(link.url, true));
                    dst.put("\" alt=\"");
                    filterHTMLAttribEscape(dst, link.text);
                    dst.put("\"");
                    if (link.title.length) {
                        dst.put(" title=\"");
                        filterHTMLAttribEscape(dst, link.title);
                        dst.put("\"");
                    }
                    dst.put(">");
                } else if (ln.length >= 2) {
                    dst.put(ln[0 .. 2]);
                    ln = ln[2 .. $];
                } else {
                    dst.put(ln[0]);
                    ln = ln[1 .. $];
                }
                break;
            case '>':
                if (this.flags & MarkdownFlags.noInlineHtml) {
                    dst.put("&gt;");
                } else
                    dst.put(ln[0]);
                ln = ln[1 .. $];
                break;
            case '<':
                string url;
                if (parseAutoLink(ln, url)) {
                    bool isEmail = url.startsWith("mailto:");
                    dst.put("<a href=\"");
                    if (isEmail) {
                        filterHTMLAllEscape(dst, url);
                    } else {
                        filterHTMLAttribEscape(dst, filterLink(url, false));
                    }
                    dst.put("\">");
                    if (isEmail) {
                        filterHTMLAllEscape(dst, url[7 .. $]);
                    } else {
                        filterHTMLEscape(
                            dst, url, HTMLEscapeFlags.escapeMinimal
                        );
                    }
                    dst.put("</a>");
                } else {
                    if (ln.startsWith("<br>")) {
                        // always support line breaks,
                        // since we embed them here ourselves!
                        dst.put("<br/>");
                        ln = ln[4 .. $];
                    } else if (ln.startsWith("<br/>")) {
                        dst.put("<br/>");
                        ln = ln[5 .. $];
                    } else {
                        if (this.flags & MarkdownFlags.noInlineHtml) {
                            dst.put("&lt;");
                        } else {
                            dst.put(ln[0]);
                        }
                        ln = ln[1 .. $];
                    }
                }
                break;
            }
        }
        if (br) {
            dst.put("<br/>");
        }
    }

    string markdownText;

    /// Controls the capabilities of the parser.
    MarkdownFlags flags = MarkdownFlags.vanillaMarkdown;

    /// Heading tags will start at this level.
    size_t headingBaseLevel = 1;

    /// Called for every link/image URL to perform arbitrary transformations.
    UrlFilterFn urlFilter;

    /***************************************************************************
     * An optional delegate to post-process code blocks and inline code.
     * Useful to e.g. add code highlighting.
     */
    ProcessCodeFn processCode = null;
}

// unittest {
//     auto text =
// `=======
// Heading
// =======

// **bold** *italic*

// List:

//   * a
//   * b
//   * c
// `;

//     writeln("~~~~~~~~~~~");
//     writeln(text);
//     writeln("~~~~~~~~~~~");
//     writeln(convertMarkdownToHTML(text));
// }

unittest {
    auto source =
        `Merged prototype.
The prototype is not locked, allowing to add more components.
  To be used it must be locked by calling EntityPrototype.lockAndTrimMemory().`;
    auto expected =
        `<p>Merged prototype.
The prototype is not locked, allowing to add more components.
  To be used it must be locked by calling EntityPrototype.lockAndTrimMemory().
</p>
`;
    string result = convertMarkdownToHTML(source);
    assert(result == expected);
}

unittest {
    auto source = `*stars* under_score_s`;
    auto expectedUnderscores = `<p><em>stars</em> under<em>score</em>s
</p>
`;
    auto expectedNoUnderscores = `<p><em>stars</em> under_score_s
</p>
`;

    string resultUnderscores = convertMarkdownToHTML(source);
    string resultNoUnderscores = convertMarkdownToHTML(
        source, MarkdownFlags.disableUnderscoreEmphasis
    );

    assert(
        resultUnderscores == expectedUnderscores,
        "'%s' != '%s'".format(resultUnderscores, expectedUnderscores)
    );
    assert(
        resultNoUnderscores == expectedNoUnderscores,
        "'%s' != '%s'".format(resultNoUnderscores, expectedNoUnderscores)
    );
}

// Unittest for code post-processing
unittest {
    auto text =
        "`inline code`" ~ `
block:

    code block
`;
    auto expected =
        `<p><code class="prettyprint">AAAAAAAAAAA</code>
block:
</p>
<pre class="prettyprint"><code>AAAAAAAAAA</code></pre>`;

    string processCode(string input) @safe nothrow {
        import std.exception : assumeWontThrow;

        // ignore newlines generated by code block processing
        input = input.filter!(c => c != '\n')
            .array
            .to!string
            .assumeWontThrow;
        return 'A'.repeat(input.length).array.to!string.assumeWontThrow;
    }

    auto settings = new MarkdownSettings;
    settings.processCode = &processCode;
    auto result = convertMarkdownToHTML(text, settings);

    auto err = format!"Unexpected code processing result:\n%s\nExpected:\n%s"(
        result, expected
    );
    assert(result == expected, err);
}

struct Section {
    size_t headingLevel;
    string caption;
    string anchor;
    Section[] subSections;
}

private {
    immutable s_blockTags = ["div", "ol", "p", "pre", "section", "table", "ul"];
}

private enum IndentType {
    White,
    Quote
}


private struct Block {
    BlockType type;
    string[] text;
    Block[] blocks;
    size_t headerLevel;

    // A human-readable toString for debugging.
    string toString() {
        return toStringNested;
    }

    // toString implementation; capable of indenting nested blocks.
    string toStringNested(uint depth = 0) {
        string indent = " ".repeat(depth * 2).joiner.array.to!string;
        return indent ~
            "%s\n".format(type) ~
            indent ~ "%s\n".format(text) ~
            blocks
                .map!((ref b) => b.toStringNested(depth + 1))
                .joiner
                .array
                .to!string ~
            indent ~
            "%s\n".format(headerLevel);
    }
}


private string[] skipText(ref Line[] lines, IndentType[] indent)
pure @safe {
    static bool matchesIndent(IndentType[] indent, IndentType[] baseIndent) {
        // Any *plain* line with a higher indent should still be a part of
        // a paragraph read by skipText(). Returning false here resulted in
        // text such as:
        // ---
        // First line
        //         Second line
        // ---
        // being interpreted as a paragraph followed by a code block, even though
        // other Markdown processors would interpret it as a single paragraph.

        // if (indent.length > baseIndent.length ) return false;
        if (indent.length > baseIndent.length) {
            return true;
        }
        if (indent != baseIndent[0 .. indent.length]) {
            return false;
        }
        sizediff_t qidx = -1;
        foreach_reverse (i, tp; baseIndent) {
            if (tp == IndentType.Quote) {
                qidx = i;
                break;
            }
        }
        if (qidx >= 0) {
            qidx = baseIndent.length - 1 - qidx;
            if (indent.length <= qidx) {
                return false;
            }
        }
        return true;
    }

    string[] ret;

    while (true) {
        ret ~= lines.front.unindent(
            min(indent.length, lines.front.indent.length)
        );
        lines.popFront();

        if (lines.empty ||
            !matchesIndent(lines.front.indent, indent) ||
            lines.front.type != LineType.Plain
        ) {
            return ret;
        }
    }
}


private Block splitTableRow(BlockType dataType = BlockType.TableData)(Line line)
pure @safe {
    static assert(
        dataType == BlockType.TableHeader || dataType == BlockType.TableData
    );

    string ln = line.text.strip();
    immutable size_t b = (ln[0 .. 2] == "| ") ? 2 : 0;
    immutable size_t e = (ln[($ - 2) .. $] == " |") ? (ln.length - 2) : ln.length;
    Block ret;
    ret.type = BlockType.TableRow;
    foreach (txt; ln[b .. e].split(" | ")) {
        Block d;
        d.text = [txt.strip(" ")];
        d.type = dataType;
        ret.blocks ~= d;
    }
    return ret;
}

private void writeBlock(R)(ref R dst,
    ref const Block block,
    LinkRef[string] links,
    scope MarkdownSettings settings) {
    final switch (block.type) {
    case BlockType.Plain:
        foreach (ln; block.text) {
            dst.put(ln);
            dst.put("\n");
        }
        foreach (b; block.blocks) {
            writeBlock(dst, b, links, settings);
        }
        break;
    case BlockType.Text:
        writeMarkdownEscaped(dst, block, links, settings);
        foreach (b; block.blocks) {
            writeBlock(dst, b, links, settings);
        }
        break;
    case BlockType.Paragraph:
        assert(block.blocks.length == 0);
        dst.put("<p>");
        writeMarkdownEscaped(dst, block, links, settings);
        dst.put("</p>\n");
        break;
    case BlockType.Header:
        assert(block.blocks.length == 0);
        auto hlvl = block.headerLevel + (settings ? settings.headingBaseLevel - 1 : 0);
        dst.formattedWrite("<h%s id=\"%s\">", hlvl, block.text[0].asSlug);
        assert(block.text.length == 1);
        writeMarkdownEscaped(dst, block.text[0], links, settings);
        dst.formattedWrite("</h%s>\n", hlvl);
        break;
    case BlockType.OList:
        dst.put("<ol>\n");
        foreach (b; block.blocks) {
            writeBlock(dst, b, links, settings);
        }
        dst.put("</ol>\n");
        break;
    case BlockType.UList:
        dst.put("<ul>\n");
        foreach (b; block.blocks) {
            writeBlock(dst, b, links, settings);
        }
        dst.put("</ul>\n");
        break;
    case BlockType.ListItem:
        dst.put("<li>");
        writeMarkdownEscaped(dst, block, links, settings);
        foreach (b; block.blocks) {
            writeBlock(dst, b, links, settings);
        }
        dst.put("</li>\n");
        break;
    case BlockType.Code:
        assert(block.blocks.length == 0);
        dst.put("<pre class=\"prettyprint\"><code>");
        if (settings.processCode is null) {
            foreach (ln; block.text) {
                filterHTMLEscape(dst, ln);
                dst.put("\n");
            }
        } else {
            auto temp = appender!string();
            foreach (ln; block.text) {
                filterHTMLEscape(temp, ln);
                temp.put("\n");
            }
            dst.put(settings.processCode(temp.data));
        }
        dst.put("</code></pre>");
        break;
    case BlockType.Quote:
        dst.put("<blockquote>");
        writeMarkdownEscaped(dst, block, links, settings);
        foreach (b; block.blocks)
            writeBlock(dst, b, links, settings);
        dst.put("</blockquote>\n");
        break;
    case BlockType.Table:
        assert(block.blocks.length > 0);
        assert(block.blocks[0].type == BlockType.TableRow);
        dst.put("<table>\n<tr>");
        foreach (b; block.blocks[0].blocks) {
            assert(b.type == BlockType.TableHeader);
            dst.put("<th>");
            writeMarkdownEscaped(dst, b.text[0], links, settings);
            dst.put("</th>");
        }
        dst.put("</tr>\n");
        if (block.blocks.length > 1) {
            foreach (row; block.blocks[1 .. $]) {
                assert(row.type == BlockType.TableRow);
                dst.put("<tr>");
                foreach (b; row.blocks) {
                    assert(b.type == BlockType.TableData);
                    dst.put("<td>");
                    writeMarkdownEscaped(dst, b.text[0], links, settings);
                    dst.put("</td>");
                }
                dst.put("</tr>\n");
            }
        }
        dst.put("</table>\n");
        break;
    case BlockType.TableRow:
    case BlockType.TableData:
    case BlockType.TableHeader:
        assert(0);
    }
}

private void writeMarkdownEscaped(R)(ref R dst,
    ref const Block block,
    in LinkRef[string] links,
    scope MarkdownSettings settings) {
    auto lines = cast(string[]) block.text;
    auto text = settings.flags & MarkdownFlags.keepLineBreaks
        ? lines.join("<br>") : lines.join("\n");
    writeMarkdownEscaped(dst, text, links, settings);
    if (lines.length) {
        dst.put("\n");
    }
}

private void writeMarkdownEscaped(R)(ref R dst,
    string ln,
    in LinkRef[string] linkrefs,
    scope MarkdownSettings settings) {
    string filterLink(string lnk, bool isImage) {
        return settings.urlFilter ? settings.urlFilter(lnk, isImage) : lnk;
    }

    bool br = ln.endsWith("  ");
    while (ln.length > 0) {
        switch (ln[0]) {
        default:
            dst.put(ln[0]);
            ln = ln[1 .. $];
            break;
        case '\\':
            if (ln.length >= 2) {
                switch (ln[1]) {
                default:
                    dst.put(ln[0 .. 2]);
                    ln = ln[2 .. $];
                    break;
                case '\'', '`', '*', '_', '{', '}', '[', ']',
                    '(', ')', '#', '+', '-', '.', '!':
                    dst.put(ln[1]);
                    ln = ln[2 .. $];
                    break;
                }
            } else {
                dst.put(ln[0]);
                ln = ln[1 .. $];
            }
            break;
        case '_':
            if (settings.flags & MarkdownFlags.disableUnderscoreEmphasis) {
                dst.put(ln[0]);
                ln = ln[1 .. $];
                break;
            }
            goto case;
        case '*':
            string text;
            if (auto em = parseEmphasis(ln, text)) {
                dst.put(em == 1 ? "<em>" : em == 2 ? "<strong>" : "<strong><em>");
                filterHTMLEscape(dst, text, HTMLEscapeFlags.escapeMinimal);
                dst.put(em == 1 ? "</em>" : em == 2 ? "</strong>" : "</em></strong>");
            } else {
                dst.put(ln[0]);
                ln = ln[1 .. $];
            }
            break;
        case '`':
            string code;
            if (parseInlineCode(ln, code)) {
                dst.put("<code class=\"prettyprint\">");
                if (settings.processCode is null) {
                    filterHTMLEscape(
                        dst, code, HTMLEscapeFlags.escapeMinimal
                    );
                } else {
                    auto temp = appender!string();
                    filterHTMLEscape(
                        temp, code, HTMLEscapeFlags.escapeMinimal
                    );
                    dst.put(settings.processCode(temp.data));
                }
                dst.put("</code>");
            } else {
                dst.put(ln[0]);
                ln = ln[1 .. $];
            }
            break;
        case '[':
            Link link;
            if (parseLink(ln, link, linkrefs)) {
                dst.put("<a href=\"");
                filterHTMLAttribEscape(dst, filterLink(link.url, false));
                dst.put("\"");
                if (link.title.length) {
                    dst.put(" title=\"");
                    filterHTMLAttribEscape(dst, link.title);
                    dst.put("\"");
                }
                dst.put(">");
                writeMarkdownEscaped(dst, link.text, linkrefs, settings);
                dst.put("</a>");
            } else {
                dst.put(ln[0]);
                ln = ln[1 .. $];
            }
            break;
        case '!':
            Link link;
            if (parseLink(ln, link, linkrefs)) {
                dst.put("<img src=\"");
                filterHTMLAttribEscape(dst, filterLink(link.url, true));
                dst.put("\" alt=\"");
                filterHTMLAttribEscape(dst, link.text);
                dst.put("\"");
                if (link.title.length) {
                    dst.put(" title=\"");
                    filterHTMLAttribEscape(dst, link.title);
                    dst.put("\"");
                }
                dst.put(">");
            } else if (ln.length >= 2) {
                dst.put(ln[0 .. 2]);
                ln = ln[2 .. $];
            } else {
                dst.put(ln[0]);
                ln = ln[1 .. $];
            }
            break;
        case '>':
            if (settings.flags & MarkdownFlags.noInlineHtml) {
                dst.put("&gt;");
            } else
                dst.put(ln[0]);
            ln = ln[1 .. $];
            break;
        case '<':
            string url;
            if (parseAutoLink(ln, url)) {
                bool isEmail = url.startsWith("mailto:");
                dst.put("<a href=\"");
                if (isEmail) {
                    filterHTMLAllEscape(dst, url);
                } else {
                    filterHTMLAttribEscape(dst, filterLink(url, false));
                }
                dst.put("\">");
                if (isEmail) {
                    filterHTMLAllEscape(dst, url[7 .. $]);
                } else {
                    filterHTMLEscape(
                        dst, url, HTMLEscapeFlags.escapeMinimal
                    );
                }
                dst.put("</a>");
            } else {
                if (ln.startsWith("<br>")) {
                    // always support line breaks,
                    // since we embed them here ourselves!
                    dst.put("<br/>");
                    ln = ln[4 .. $];
                } else if (ln.startsWith("<br/>")) {
                    dst.put("<br/>");
                    ln = ln[5 .. $];
                } else {
                    if (settings.flags & MarkdownFlags.noInlineHtml) {
                        dst.put("&lt;");
                    } else {
                        dst.put(ln[0]);
                    }
                    ln = ln[1 .. $];
                }
            }
            break;
        }
    }
    if (br) {
        dst.put("<br/>");
    }
}


private bool isLineBlank(string ln)
pure @safe {
    return allOf(ln, " \t");
}


private bool isSetextHeaderLine(string ln, char subHeaderChar) pure @safe {
    ln = stripLeft(ln);
    if (ln.length < 1) {
        return false;
    }
    if (ln[0] == subHeaderChar) {
        while (!ln.empty && ln.front == subHeaderChar) {
            ln.popFront();
        }
        return allOf(ln, " \t");
    }
    return false;
}


private bool isAtxHeaderLine(string ln) pure @safe {
    ln = stripLeft(ln);
    size_t i = 0;
    while (i < ln.length && ln[i] == '#') {
        i++;
    }
    if (i < 1 || i > 6 || i >= ln.length) {
        return false;
    }
    return ln[i] == ' ';
}


private bool isHlineLine(string ln) pure @safe {
    if (allOf(ln, " -") && count(ln, '-') >= 3) {
        return true;
    } else if (allOf(ln, " *") && count(ln, '*') >= 3) {
        return true;
    } else if (allOf(ln, " _") && count(ln, '_') >= 3) {
        return true;
    }
    return false;
}


private bool isQuoteLine(string ln) pure @safe {
    return ln.stripLeft().startsWith(">");
}


private size_t getQuoteLevel(string ln) pure @safe {
    size_t level = 0;
    ln = stripLeft(ln);
    while (ln.length > 0 && ln[0] == '>') {
        level++;
        ln = stripLeft(ln[1 .. $]);
    }
    return level;
}


private bool isUListLine(string ln) pure @safe {
    ln = stripLeft(ln);
    if (ln.length < 2) {
        return false;
    }
    if (!canFind("*+-", ln[0])) {
        return false;
    }
    if (ln[1] != ' ' && ln[1] != '\t') {
        return false;
    }
    return true;
}


private bool isOListLine(string ln) pure @safe {
    ln = stripLeft(ln);
    if (ln.length < 1) {
        return false;
    }
    if (ln[0] < '0' || ln[0] > '9') {
        return false;
    }
    ln = ln[1 .. $];
    while (ln.length > 0 && ln[0] >= '0' && ln[0] <= '9') {
        ln = ln[1 .. $];
    }
    if (ln.length < 2) {
        return false;
    }
    if (ln[0] != '.') {
        return false;
    }
    if (ln[1] != ' ' && ln[1] != '\t') {
        return false;
    }
    return true;
}


private bool isTableRowLine(string ln) pure @safe {
    return
        ln.indexOf(" | ") >= 0 &&
        !ln.isOListLine &&
        !ln.isUListLine &&
        !ln.isAtxHeaderLine;
}


private string removeListPrefix(string str, LineType tp) pure @safe {
    switch (tp) {
    default:
        assert(false);
    case LineType.OList: // skip bullets and output using normal escaping
        auto idx = str.indexOfCT('.');
        assert(idx > 0);
        return str[idx + 1 .. $].stripLeft();
    case LineType.UList:
        return stripLeft(str.stripLeft()[1 .. $]);
    }
}


private auto parseHtmlBlockLine(string ln) pure @safe {
    struct HtmlBlockInfo {
        bool isHtmlBlock;
        string tagName;
        bool open;
    }

    HtmlBlockInfo ret;
    ret.isHtmlBlock = false;
    ret.open = true;

    ln = strip(ln);
    if (ln.length < 3) {
        return ret;
    }
    if (ln[0] != '<') {
        return ret;
    }
    if (ln[1] == '/') {
        ret.open = false;
        ln = ln[1 .. $];
    }
    if (!std.ascii.isAlpha(ln[1])) {
        return ret;
    }
    ln = ln[1 .. $];
    size_t idx = 0;
    while (idx < ln.length && ln[idx] != ' ' && ln[idx] != '>') {
        idx++;
    }
    ret.tagName = ln[0 .. idx];
    ln = ln[idx .. $];

    auto eidx = ln.indexOf('>');
    if (eidx < 0) {
        return ret;
    }
    if (eidx != ln.length-1) {
        return ret;
    }

    if (!s_blockTags.canFind(ret.tagName)) {
        return ret;
    }

    ret.isHtmlBlock = true;
    return ret;
}


private bool isHtmlBlockLine(string ln) pure @safe {
    auto bi = parseHtmlBlockLine(ln);
    return bi.isHtmlBlock && bi.open;
}


private bool isHtmlBlockCloseLine(string ln) pure @safe {
    auto bi = parseHtmlBlockLine(ln);
    return bi.isHtmlBlock && !bi.open;
}


private bool isCodeBlockDelimiter(string ln) pure @safe {
    return ln.startsWith("```");
}

// private string getHtmlTagName(string ln) pure @safe {
//     return parseHtmlBlockLine(ln).tagName;
// }

private bool isLineIndented(string ln) pure @safe {
    return ln.startsWith("\t") || ln.startsWith("    ");
}

// private string unindentLine(string ln) pure @safe {
//     if (ln.startsWith("\t")) return ln[1 .. $];
//     if (ln.startsWith("    ")) return ln[4 .. $];
//     assert(false);
// }

private int parseEmphasis(ref string str, ref string text) pure @safe {
    string pstr = str;
    if (pstr.length < 3)
        return false;

    string ctag;
    if (pstr.startsWith("***"))
        ctag = "***";
    else if (pstr.startsWith("**"))
        ctag = "**";
    else if (pstr.startsWith("*"))
        ctag = "*";
    else if (pstr.startsWith("___"))
        ctag = "___";
    else if (pstr.startsWith("__"))
        ctag = "__";
    else if (pstr.startsWith("_"))
        ctag = "_";
    else
        return false;

    pstr = pstr[ctag.length .. $];

    auto cidx = () @trusted { return pstr.indexOf(ctag); }();
    if (cidx < 1)
        return false;

    text = pstr[0 .. cidx];

    str = pstr[cidx + ctag.length .. $];
    return cast(int) ctag.length;
}


private bool parseInlineCode(ref string str, ref string code) pure @safe {
    string pstr = str;
    if (pstr.length < 3)
        return false;
    string ctag;
    if (pstr.startsWith("``"))
        ctag = "``";
    else if (pstr.startsWith("`"))
        ctag = "`";
    else
        return false;
    pstr = pstr[ctag.length .. $];

    auto cidx = () @trusted { return pstr.indexOf(ctag); }();
    if (cidx < 1)
        return false;

    code = pstr[0 .. cidx];
    str = pstr[cidx + ctag.length .. $];
    return true;
}


private bool parseLink(
    ref string str, ref Link dst, in LinkRef[string] linkrefs
) pure @safe {
    string pstr = str;
    if (pstr.length < 3)
        return false;
    // ignore img-link prefix
    if (pstr[0] == '!')
        pstr = pstr[1 .. $];

    // parse the text part [text]
    if (pstr[0] != '[')
        return false;
    auto cidx = pstr.matchBracket();
    if (cidx < 1)
        return false;
    string refid;
    dst.text = pstr[1 .. cidx];
    pstr = pstr[cidx + 1 .. $];

    // parse either (link '['"title"']') or '[' ']'[refid]
    if (pstr.length < 2)
        return false;
    if (pstr[0] == '(') {
        cidx = pstr.matchBracket();
        if (cidx < 1)
            return false;
        auto inner = pstr[1 .. cidx];
        immutable qidx = inner.indexOfCT('"');
        if (qidx > 1 && std.ascii.isWhite(inner[qidx - 1])) {
            dst.url = inner[0 .. qidx].stripRight();
            immutable len = inner[qidx .. $].lastIndexOf('"');
            if (len == 0)
                return false;
            assert(len > 0);
            dst.title = inner[qidx + 1 .. qidx + len];
        } else {
            dst.url = inner.stripRight();
            dst.title = null;
        }
        if (dst.url.startsWith("<") && dst.url.endsWith(">"))
            dst.url = dst.url[1 .. $ - 1];
        pstr = pstr[cidx + 1 .. $];
    } else {
        if (pstr[0] == ' ')
            pstr = pstr[1 .. $];
        if (pstr[0] != '[')
            return false;
        pstr = pstr[1 .. $];
        cidx = pstr.indexOfCT(']');
        if (cidx < 0)
            return false;
        if (cidx == 0)
            refid = dst.text;
        else
            refid = pstr[0 .. cidx];
        pstr = pstr[cidx + 1 .. $];
    }

    if (refid.length > 0) {
        auto pr = toLower(refid) in linkrefs;
        if (!pr) {
            // debug if (!__ctfe) logDebug("[LINK REF NOT FOUND: '%s'", refid);
            return false;
        }
        dst.url = pr.url;
        dst.title = pr.title;
    }

    str = pstr;
    return true;
}


/* UNITTESTS */


@safe unittest {
    static void testLink(string s, Link exp, in LinkRef[string] refs) {
        Link link;
        assert(parseLink(s, link, refs), s);
        assert(link == exp);
    }

    LinkRef[string] refs;
    refs["ref"] = LinkRef("ref", "target", "title");

    testLink(`[link](target)`, Link("link", "target"), null);
    testLink(`[link](target "title")`, Link("link", "target", "title"), null);
    testLink(`[link](target  "title")`, Link("link", "target", "title"), null);
    testLink(`[link](target "title"  )`, Link("link", "target", "title"), null);

    testLink(`[link](target)`, Link("link", "target"), null);
    testLink(`[link](target "title")`, Link("link", "target", "title"), null);

    testLink(`[link][ref]`, Link("link", "target", "title"), refs);
    testLink(`[ref][]`, Link("ref", "target", "title"), refs);

    testLink(`[link[with brackets]](target)`, Link("link[with brackets]", "target"), null);
    testLink(`[link[with brackets]][ref]`, Link("link[with brackets]", "target", "title"), refs);

    testLink(`[link](/target with spaces )`, Link("link", "/target with spaces"), null);
    testLink(`[link](/target with spaces "title")`, Link("link", "/target with spaces", "title"), null);

    testLink(`[link](white-space  "around title" )`, Link("link", "white-space", "around title"), null);
    testLink(`[link](tabs    "around title"    )`, Link("link", "tabs", "around title"), null);

    testLink(`[link](target "")`, Link("link", "target", ""), null);
    testLink(`[link](target-no-title"foo" )`, Link("link", "target-no-title\"foo\"", ""), null);

    testLink(`[link](<target>)`, Link("link", "target"), null);

    auto failing = [
        `text`, `[link](target`, `[link]target)`, `[link]`,
        `[link(target)`, `link](target)`, `[link] (target)`,
        `[link][noref]`, `[noref][]`
    ];
    Link link;
    foreach (s; failing)
        assert(!parseLink(s, link, refs), s);
}

private bool parseAutoLink(ref string str, ref string url)
pure @safe {
    string pstr = str;
    if (pstr.length < 3)
        return false;
    if (pstr[0] != '<')
        return false;
    pstr = pstr[1 .. $];
    auto cidx = pstr.indexOf('>');
    if (cidx < 0)
        return false;
    url = pstr[0 .. cidx];
    if (anyOf(url, " \t"))
        return false;
    if (!anyOf(url, ":@"))
        return false;
    str = pstr[cidx + 1 .. $];
    if (url.indexOf('@') > 0)
        url = "mailto:" ~ url;
    return true;
}

/*******************************************************************************
 * Generates an identifier suitable to use as within a URL.
 *
 * The resulting string will contain only ASCII lower case alphabetic or
 * numeric characters, as well as dashes (-). Every sequence of
 * non-alphanumeric characters will be replaced by a single dash. No dashes
 * will be at either the front or the back of the result string.
 */
auto asSlug(R)(R text) if (isInputRange!R && is(typeof(R.init.front) == dchar)) {
    static struct SlugRange {
        private {
            R _input;
            bool _dash;
        }

        this(R input) {
            _input = input;
            skipNonAlphaNum();
        }

        @property bool empty() const {
            return _dash ? false : _input.empty;
        }

        @property char front() const {
            if (_dash)
                return '-';

            char r = cast(char) _input.front;
            if (r >= 'A' && r <= 'Z')
                return cast(char)(r + ('a' - 'A'));
            return r;
        }

        void popFront() {
            if (_dash) {
                _dash = false;
                return;
            }

            _input.popFront();
            auto na = skipNonAlphaNum();
            if (na && !_input.empty)
                _dash = true;
        }

        private bool skipNonAlphaNum() {
            bool have_skipped = false;
            while (!_input.empty) {
                switch (_input.front) {
                default:
                    _input.popFront();
                    have_skipped = true;
                    break;
                case 'a': .. case 'z':
                case 'A': .. case 'Z':
                case '0': .. case '9':
                    return have_skipped;
                }
            }
            return have_skipped;
        }
    }

    return SlugRange(text);
}

unittest {
    import std.algorithm : equal;

    assert("".asSlug.equal(""));
    assert(".,-".asSlug.equal(""));
    assert("abc".asSlug.equal("abc"));
    assert("aBc123".asSlug.equal("abc123"));
    assert("....aBc...123...".asSlug.equal("abc-123"));
}

private struct Link {
    string text;
    string url;
    string title;
}

@safe unittest { // alt and title attributes
    assert(convertMarkdownToHTML("![alt](http://example.org/image)")
            == "<p><img src=\"http://example.org/image\" alt=\"alt\">\n</p>\n");
    assert(convertMarkdownToHTML("![alt](http://example.org/image \"Title\")")
            == "<p><img src=\"http://example.org/image\" alt=\"alt\" title=\"Title\">\n</p>\n");
}

@safe unittest { // complex links
    assert(convertMarkdownToHTML("their [install\ninstructions](<http://www.brew.sh>) and")
            == "<p>their <a href=\"http://www.brew.sh\">install\ninstructions</a> and\n</p>\n");
    assert(convertMarkdownToHTML("[![Build Status](https://travis-ci.org/rejectedsoftware/vibe.d.png)](https://travis-ci.org/rejectedsoftware/vibe.d)")
            == "<p><a href=\"https://travis-ci.org/rejectedsoftware/vibe.d\"><img src=\"https://travis-ci.org/rejectedsoftware/vibe.d.png\" alt=\"Build Status\"></a>\n</p>\n");
}

@safe unittest { // check CTFE-ability
    enum res = convertMarkdownToHTML("### some markdown\n[foo][]\n[foo]: /bar");
    assert(
        res == "<h3 id=\"some-markdown\"> some markdown</h3>\n<p><a href=\"/bar\">foo</a>\n</p>\n", res);
}

@safe unittest { // correct line breaks in restrictive mode
    auto res = convertMarkdownToHTML("hello\nworld", MarkdownFlags.forumDefault);
    assert(res == "<p>hello<br/>world\n</p>\n", res);
}

/*@safe unittest { // code blocks and blockquotes
    assert(convertMarkdownToHTML("\tthis\n\tis\n\tcode") ==
        "<pre><code>this\nis\ncode</code></pre>\n");
    assert(convertMarkdownToHTML("    this\n    is\n    code") ==
        "<pre><code>this\nis\ncode</code></pre>\n");
    assert(convertMarkdownToHTML("    this\n    is\n\tcode") ==
        "<pre><code>this\nis</code></pre>\n<pre><code>code</code></pre>\n");
    assert(convertMarkdownToHTML("\tthis\n\n\tcode") ==
        "<pre><code>this\n\ncode</code></pre>\n");
    assert(convertMarkdownToHTML("\t> this") ==
        "<pre><code>&gt; this</code></pre>\n");
    assert(convertMarkdownToHTML(">     this") ==
        "<blockquote><pre><code>this</code></pre></blockquote>\n");
    assert(convertMarkdownToHTML(">     this\n    is code") ==
        "<blockquote><pre><code>this\nis code</code></pre></blockquote>\n");
}*/

@safe unittest { // test simple border-less table
    auto res = convertMarkdownToHTML(
        "Col 1 | Col 2 | Col 3\n -- | -- | --\n val 1 | val 2 | val 3\n *val 4* | val 5 | value 6",
        MarkdownFlags.supportTables
    );
    assert(res == "<table>\n<tr><th>Col 1</th><th>Col 2</th><th>Col 3</th></tr>\n<tr><td>val 1</td><td>val 2</td><td>val 3</td></tr>\n<tr><td><em>val 4</em></td><td>val 5</td><td>value 6</td></tr>\n</table>\n", res);
}

@safe unittest { // test simple border'ed table
    auto res = convertMarkdownToHTML(
        "| Col 1 | Col 2 | Col 3 |\n| -- | -- | -- |\n| val 1 | val 2 | val 3 |\n| *val 4* | val 5 | value 6 |",
        MarkdownFlags.supportTables
    );
    assert(res == "<table>\n<tr><th>Col 1</th><th>Col 2</th><th>Col 3</th></tr>\n<tr><td>val 1</td><td>val 2</td><td>val 3</td></tr>\n<tr><td><em>val 4</em></td><td>val 5</td><td>value 6</td></tr>\n</table>\n", res);
}

@safe unittest {
    string input = `
Table:

ID  | Name  | Address
 -- | ----  | ---------
 1  | Foo   | Somewhere
 2  | Bar   | Nowhere `;
    auto res = convertMarkdownToHTML(input, MarkdownFlags.supportTables);
    auto exp = "<p>Table:\n</p>\n<table>\n<tr><th>ID</th><th>Name</th><th>Address</th></tr>\n<tr><td>1</td><td>Foo</td><td>Somewhere</td></tr>\n<tr><td>2</td><td>Bar</td><td>Nowhere</td></tr>\n</table>\n";
    assert(res == exp, res);
}

package:

/// Function for work with HTML.

/*******************************************************************************
 * Writes the HTML escaped version of a given string to an output range.
 */
void filterHTMLEscape(R, S)(
    ref R dst,
    S str,
    HTMLEscapeFlags flags = HTMLEscapeFlags.escapeNewline
)
if (isOutputRange!(R, dchar) && isInputRange!S) {
    HTMLEscapeFlags f;
    for (; !str.empty; str.popFront()) {
        filterHTMLEscape(dst, str.front, flags);
    }
}

/*******************************************************************************
 * Writes the HTML escaped version of a given string to an output range
 * (also escapes double quotes).
 */
void filterHTMLAttribEscape(R, S)(ref R dst, S str)
        if (isOutputRange!(R, dchar) && isInputRange!S) {
    for (; !str.empty; str.popFront()) {
        filterHTMLEscape(
            dst,
            str.front,
            HTMLEscapeFlags.escapeNewline | HTMLEscapeFlags.escapeQuotes
        );
    }
}

/*******************************************************************************
 * Writes the HTML escaped version of a given string to an output range
 * (escapes every character).
 */
void filterHTMLAllEscape(R, S)(ref R dst, S str)
        if (isOutputRange!(R, dchar) && isInputRange!S) {
    for (; !str.empty; str.popFront()) {
        dst.put("&#");
        dst.put(to!string(cast(uint) str.front));
        dst.put(';');
    }
}

/*******************************************************************************
 * Writes the HTML escaped version of a character to an output range.
 */
void filterHTMLEscape(R)(ref R dst,
    dchar ch,
    HTMLEscapeFlags flags = HTMLEscapeFlags.escapeNewline) {
    switch (ch) {
    default:
        if (flags & HTMLEscapeFlags.escapeUnknown) {
            dst.put("&#");
            dst.put(to!string(cast(uint) ch));
            dst.put(';');
        } else
            dst.put(ch);
        break;
    case '"':
        if (flags & HTMLEscapeFlags.escapeQuotes)
            dst.put("&quot;");
        else
            dst.put('"');
        break;
    case '\'':
        if (flags & HTMLEscapeFlags.escapeQuotes)
            dst.put("&#39;");
        else
            dst.put('\'');
        break;
    case '\r', '\n':
        if (flags & HTMLEscapeFlags.escapeNewline) {
            dst.put("&#");
            dst.put(to!string(cast(uint) ch));
            dst.put(';');
        } else
            dst.put(ch);
        break;
    case 'a': .. case 'z':
        goto case;
    case 'A': .. case 'Z':
        goto case;
    case '0': .. case '9':
        goto case;
    case ' ', '\t', '-', '_', '.', ':', ',', ';',
        '#', '+', '*', '?', '=', '(', ')', '/', '!',
        '%', '{', '}', '[', ']', '`', '´', '$', '^', '~':
        dst.put(cast(char) ch);
        break;
    case '<':
        dst.put("&lt;");
        break;
    case '>':
        dst.put("&gt;");
        break;
    case '&':
        dst.put("&amp;");
        break;
    }
}

/// Flags for HTML-escaping some symbols.
enum HTMLEscapeFlags {
    escapeMinimal = 0,
    escapeQuotes = 1 << 0,
    escapeNewline = 1 << 1,
    escapeUnknown = 1 << 2
}

/// Functions for work with string data

/*******************************************************************************
 * Checks if all characters in 'str' are contained in 'chars'.
 */
bool allOf(string str, string chars)
@safe pure {
    foreach (dchar ch; str) {
        if (!chars.canFind(ch)) {
            return false;
        }
    }
    return true;
}

ptrdiff_t indexOfCT(Char)(in Char[] s, dchar c, CaseSensitive cs = CaseSensitive.yes)
@safe pure {
    if (__ctfe) {
        if (cs == CaseSensitive.yes) {
            foreach (i, dchar ch; s) {
                if (ch == c) {
                    return i;
                }
            }
        } else {
            c = std.uni.toLower(c);
            foreach (i, dchar ch; s) {
                if (std.uni.toLower(ch) == c) {
                    return i;
                }
            }
        }
        return -1;
    }
    return std.string.indexOf(s, c, cs);
}

/*******************************************************************************
 * Checks if any character in 'str' is contained in 'chars'.
 */
bool anyOf(string str, string chars)
@safe pure {
    foreach (ch; str) {
        if (chars.canFind(ch)) {
            return true;
        }
    }
    return false;
}

/*******************************************************************************
 * Finds the closing bracket (works with any of '[', '$(LPAREN)', '<', '{').
 *
 * Params:
 *     str = input string
 *     nested = whether to skip nested brackets
 * Returns:
 *     The index of the closing bracket or -1 for unbalanced strings
 *     and strings that don't start with a bracket.
 */
sizediff_t matchBracket(string str, bool nested = true)
@safe pure nothrow {
    if (str.length < 2)
        return -1;

    char open = str[0], close = void;
    switch (str[0]) {
    case '[':
        close = ']';
        break;
    case '(':
        close = ')';
        break;
    case '<':
        close = '>';
        break;
    case '{':
        close = '}';
        break;
    default:
        return -1;
    }

    size_t level = 1;
    foreach (i, char c; str[1 .. $]) {
        if (nested && c == open)
            ++level;
        else if (c == close)
            --level;
        if (level == 0)
            return i + 1;
    }
    return -1;
}

////////////////////////////////////////////////////////////////////////////////
////                        DEPRECATED FUNCTIONS                            ////
////////////////////////////////////////////////////////////////////////////////

/*******************************************************************************
 * Returns the hierarchy of sections.
 */
Section[] getMarkdownOutline(string markdown_source,
    scope MarkdownSettings settings = null) {
    if (!settings)
        settings = new MarkdownSettings;
    auto all_lines = splitLines(markdown_source);
    auto lines = parseLines(all_lines, settings);
    Block root_block;
    parseBlocks(root_block, lines, null, settings);
    Section root;

    foreach (ref sb; root_block.blocks) {
        if (sb.type != BlockType.Header) {
            continue;
        }
        auto s = &root;
        while (true) {
            if (s.subSections.length == 0)
                break;
            if (s.subSections[$ - 1].headingLevel >= sb.headerLevel)
                break;
            s = &s.subSections[$ - 1];
        }
        s.subSections ~= Section(
            sb.headerLevel, sb.text[0], sb.text[0].asSlug.to!string
        );
    }

    return root.subSections;
}
///
unittest {
    auto mdText = "## first\n## second\n### third\n# fourth\n### fifth";
    auto expected = [
        Section(2, " first", "first"),
        Section(2, " second", "second", [Section(3, " third", "third")]),
        Section(1, " fourth", "fourth", [Section(3, " fifth", "fifth")])
    ];
    assert(getMarkdownOutline(mdText) == expected);
}

private Line[] parseLines(ref string[] lines, scope MarkdownSettings settings)
pure @safe {
    Line[] ret;
    char subHeaderChar = '-';
    while (!lines.empty) {
        auto ln = lines.front;
        lines.popFront();

        Line lninfo;
        lninfo.text = ln;

        void determineIndent() {
            while (ln.length > 0) {
                if (ln[0] == '\t') {
                    lninfo.indent ~= IndentType.White;
                    ln.popFront();
                } else if (ln.startsWith("    ")) {
                    lninfo.indent ~= IndentType.White;
                    ln.popFrontN(4);
                } else {
                    ln = ln.stripLeft();
                    if (ln.startsWith(">")) {
                        lninfo.indent ~= IndentType.Quote;
                        ln.popFront();
                    } else {
                        break;
                    }
                }
            }
            lninfo.unindented = ln;
        }

        determineIndent();

        if ((settings.flags & MarkdownFlags.backtickCodeBlocks)
            && isCodeBlockDelimiter(ln)) {
            lninfo.type = LineType.CodeBlockDelimiter;
        } else if (isAtxHeaderLine(ln)) {
            lninfo.type = LineType.AtxHeader;
        } else if (isSetextHeaderLine(ln, subHeaderChar)) {
            lninfo.type = LineType.SetextHeader;
        } else if ((settings.flags & MarkdownFlags.supportTables)
            && isTableRowLine(ln)) {
            lninfo.type = LineType.Table;
        } else if (isHlineLine(ln)) {
            lninfo.type = LineType.Hline;
        } else if (isOListLine(ln)) {
            lninfo.type = LineType.OList;
        } else if (isUListLine(ln)) {
            lninfo.type = LineType.UList;
        } else if (isLineBlank(ln)) {
            lninfo.type = LineType.Blank;
        } else if (!(settings.flags & MarkdownFlags.noInlineHtml)
            && isHtmlBlockLine(ln)) {
            lninfo.type = LineType.HtmlBlock;
        } else
            lninfo.type = LineType.Plain;

        ret ~= lninfo;
    }
    return ret;
}

private void parseBlocks(ref Block root,
    ref Line[] lines,
    IndentType[] baseIndent,
    scope MarkdownSettings settings)
pure @safe {
    if (baseIndent.length == 0) {
        root.type = BlockType.Text;
    } else if (baseIndent[$ - 1] == IndentType.Quote) {
        root.type = BlockType.Quote;
    }

    while (!lines.empty) {
        auto ln = lines.front;

        if (ln.type == LineType.Blank) {
            lines.popFront();
            continue;
        }

        if (ln.indent != baseIndent) {
            if (ln.indent.length < baseIndent.length ||
                ln.indent[0 .. baseIndent.length] != baseIndent) {
                return;
            }

            auto cindent = baseIndent ~ IndentType.White;
            if (ln.indent == cindent) {
                Block cblock;
                cblock.type = BlockType.Code;
                while (!lines.empty &&
                    lines.front.indent.length >= cindent.length &&
                    lines.front.indent[0 .. cindent.length] == cindent) {
                    cblock.text ~= lines.front.unindent(cindent.length);
                    lines.popFront();
                }
                root.blocks ~= cblock;
            } else {
                Block subblock;
                parseBlocks(subblock,
                    lines,
                    ln.indent[0 .. baseIndent.length + 1],
                    settings);
                root.blocks ~= subblock;
            }
        } else {
            Block b;
            void processPlain() {
                b.type = BlockType.Paragraph;
                b.text = skipText(lines, baseIndent);
            }

            final switch (ln.type) {
            case LineType.Undefined:
                assert(false);
            case LineType.Blank:
                assert(false);
            case LineType.Plain:
                if (lines.length >= 2 &&
                    lines[1].type == LineType.SetextHeader) {
                    auto setln = lines[1].unindented;
                    b.type = BlockType.Header;
                    b.text = [ln.unindented];
                    b.headerLevel = setln.strip()[0] == '=' ? 1 : 2;
                    lines.popFrontN(2);
                } else {
                    processPlain();
                }
                break;
            case LineType.Hline:
                b.type = BlockType.Plain;
                b.text = ["<hr>"];
                lines.popFront();
                break;
            case LineType.AtxHeader:
                b.type = BlockType.Header;
                string hl = ln.unindented;
                b.headerLevel = 0;
                while (hl.length > 0 && hl[0] == '#') {
                    b.headerLevel++;
                    hl = hl[1 .. $];
                }
                while (hl.length > 0 && (hl[$ - 1] == '#' || hl[$ - 1] == ' '))
                    hl = hl[0 .. $ - 1];
                b.text = [hl];
                lines.popFront();
                break;
            case LineType.SetextHeader:
                lines.popFront();
                break;
            case LineType.UList:
            case LineType.OList:
                b.type = ln.type == LineType.UList ? BlockType.UList : BlockType.OList;
                auto itemindent = baseIndent ~ IndentType.White;
                bool firstItem = true, paraMode = false;
                while (!lines.empty && lines.front.type == ln.type &&
                    lines.front.indent == baseIndent) {
                    Block itm;
                    itm.text = skipText(lines, itemindent);
                    itm.text[0] = removeListPrefix(itm.text[0], ln.type);

                    // emit <p></p> if there are blank lines between the items
                    if (firstItem && !lines.empty &&
                        lines.front.type == LineType.Blank) {
                        paraMode = true;
                    }
                    firstItem = false;
                    if (paraMode) {
                        Block para;
                        para.type = BlockType.Paragraph;
                        para.text = itm.text;
                        itm.blocks ~= para;
                        itm.text = null;
                    }

                    parseBlocks(itm, lines, itemindent, settings);
                    itm.type = BlockType.ListItem;
                    b.blocks ~= itm;
                }
                break;
            case LineType.HtmlBlock:
                int nestlevel = 0;
                auto starttag = parseHtmlBlockLine(ln.unindented);
                if (!starttag.isHtmlBlock || !starttag.open)
                    break;

                b.type = BlockType.Plain;
                while (!lines.empty) {
                    if (lines.front.indent.length < baseIndent.length) {
                        break;
                    }
                    if (lines.front.indent[0 .. baseIndent.length] != baseIndent) {
                        break;
                    }

                    auto str = lines.front.unindent(baseIndent.length);
                    auto taginfo = parseHtmlBlockLine(str);
                    b.text ~= lines.front.unindent(baseIndent.length);
                    lines.popFront();
                    if (taginfo.isHtmlBlock
                        && taginfo.tagName == starttag.tagName) {
                        nestlevel += taginfo.open ? 1 : -1;
                    }
                    if (nestlevel <= 0) {
                        break;
                    }
                }
                break;
            case LineType.CodeBlockDelimiter:
                lines.popFront(); // TODO: get language from line
                b.type = BlockType.Code;
                while (!lines.empty) {
                    if (lines.front.indent.length < baseIndent.length) {
                        break;
                    }
                    if (lines.front.indent[0 .. baseIndent.length] != baseIndent) {
                        break;
                    }
                    if (lines.front.type == LineType.CodeBlockDelimiter) {
                        lines.popFront();
                        break;
                    }
                    b.text ~= lines.front.unindent(baseIndent.length);
                    lines.popFront();
                }
                break;
            case LineType.Table:
                lines.popFront();
                // Can this be a valid table (is there a next line that could be a header separator)?
                if (lines.empty) {
                    processPlain();
                    break;
                }
                Line lnNext = lines.front;
                immutable bool isTableHeader = (
                    (lnNext.type == LineType.Table)
                        && (lnNext.text.indexOf(" -") >= 0)
                        && (lnNext.text.indexOf("- ") >= 0)
                        && lnNext.text.allOf("-:| ")
                );
                if (!isTableHeader) {
                    // Not a valid table header, so let's assume it's plain markdown
                    processPlain();
                    break;
                }
                b.type = BlockType.Table;
                // Parse header
                b.blocks ~= splitTableRow!(BlockType.TableHeader)(ln);
                // Parse table rows
                lines.popFront();
                while (!lines.empty) {
                    ln = lines.front;
                    if (ln.type != LineType.Table)
                        break; // not a table row, so let's assume it's the end of the table
                    b.blocks ~= splitTableRow(ln);
                    lines.popFront();
                }
                break;
            }
            root.blocks ~= b;
        }
    }
}
